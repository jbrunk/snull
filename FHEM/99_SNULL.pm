#----------------------------------------------------------------------
#
#  This script is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  The GNU General Public License can be found at
#  http://www.gnu.org/copyleft/gpl.html.
#  A copy is found in the textfile GPL.txt and important notices to the license
#  from the author is found in LICENSE.txt distributed with these scripts.
#
#  This script is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#
#  99_SNULL.pm - a simple S0 Counter
#
#  based on 23_WEBIO.pm
#
#----------------------------------------------------------------------

package main;

use strict;
use warnings;
use Data::Dumper;
use LWP::UserAgent;
use HTTP::Request;

#----------------------------------------------------------------------

sub SNULL_Initialize($)
{
  my ($hash) = @_;

  $hash->{DefFn}     = "SNULL_Define";
  $hash->{UndefFn}   = "SNULL_Undef";
  $hash->{AttrList}  = "loglevel:0,1,2,3,4,5,6 factor1 factor2 factor3 factor4 factor5 factor6";
}

#----------------------------------------------------------------------

sub SNULL_Define($$)
{
  my ($hash, $def) = @_;
  my $name = $hash->{NAME};
#  my @a = split("[ \t][ \t]*", $def);
  my @a = split('\s+', $def);


  return "Wrong syntax: use define <name> SNULL <host|ip> [<port> [<interval>] [[factor]]]" if(int(@a) < 3 || int(@a) > 6);
  
  $hash->{HOST} = $a[2];
  $hash->{PORT} = $a[3] || 80;
  $hash->{INTERVAL} = $a[4] || 300;
  $hash->{FACTOR} = $a[5] || 1000;
  $hash->{COUNT} = -1;
  $hash->{MILLIS} = -1;
  
  Log 4, "SNULL_Define: host=$hash->{HOST}, port=$hash->{PORT}, interval=$hash->{INTERVAL}, factor=$hash->{FACTOR}";

  $attr{$name}{factor1} = $hash->{FACTOR};
  $attr{$name}{factor2} = $hash->{FACTOR};
  $attr{$name}{factor3} = $hash->{FACTOR};
  $attr{$name}{factor4} = $hash->{FACTOR};
  $attr{$name}{factor5} = $hash->{FACTOR};
  $attr{$name}{factor6} = $hash->{FACTOR};
 
  # start Timer
  InternalTimer(gettimeofday()+$hash->{INTERVAL}, "SNULL_GetStatus", $hash, 0);
 
  return undef;
}

#----------------------------------------------------------------------

sub SNULL_Undef($$)
{
    my ($hash, $name) = @_;
    
    Log 4, "SNULL_Undef called";
    #delete($modules{SNULL}{defptr}{$hash->{CODE}});
    RemoveInternalTimer($hash);
    
    return undef;
}

#----------------------------------------------------------------------

sub SNULL_GetStatus($)
{
  my ($hash) = @_;
  my $name = $hash->{NAME};
  my $err_log = '';
  my $line;
  my $URL="http://$hash->{HOST}:$hash->{PORT}/";

  # renew Timer
  InternalTimer(gettimeofday()+$hash->{INTERVAL}, "SNULL_GetStatus", $hash, 1);
  
  # perform simple HTTP Get Request
  my $agent = LWP::UserAgent->new(env_proxy => 1, keep_alive => 0, timeout => 10);
  my $header = HTTP::Request->new(GET => $URL);
  my $request = HTTP::Request->new('GET', $URL, $header);
  my $response = $agent->request($request);

  $err_log.= "Can't get $URL - ".$response->status_line unless $response->is_success;

  if ( $err_log ne "" )
  {
        Log 2, "SNULL ".$err_log;
	#$hash->{STATE} = "offline";
        return("");
  }

  #$hash->{STATE} = "online";
  
  my $body = $response->content;
  $body =~ s/\R//g;
  Log 4, "SNULL_GetStatus: $URL $body";

  # Raw Data:
  # count: 2 millis: 1001 s0: 11 13 24 34 0 0
  $hash->{RAW_DATA} = $body;
  my @values = split(/ /, $body);
  my $nvalues = @values; # should by always 11
  Log 4, "SNULL_GetStatus: nvalues = $nvalues, \@values = \'" . join(' ', @values) . "\'"; 
  
  my ($count, $millis, $s0_1, $s0_2, $s0_3, $s0_4, $s0_5, $s0_6) =
  	($values[1], $values[3], $values[5], $values[6], $values[7],
  	$values[8], $values[9], $values[10]);
  my $time = TimeNow();
  
  $hash->{COUNT} = $count;
  $hash->{MILLIS} = $millis;
  
  # translate raw counts
  my ($factor, $value);
    
  readingsBeginUpdate($hash);
 
  # 1000 impulses = 1 kWh, 1000 W in 60 min / 3600 secs
  # eg. 1 Impuls / sec => 300 Impulses in 5 mins
  # impulses / (millis / 1000) * 3600 / $factor
  
  $factor = $attr{$name}{factor1} || ${hash}->{FACTOR};
  $value = $s0_1 / ($millis / 1000) * 3600 / $factor;
  readingsBulkUpdate($hash, 's0_1', sprintf("%0.3f", $value));
  
  $factor = $attr{$name}{factor2} || ${hash}->{FACTOR};
  $value = $s0_2 / ($millis / 1000) * 3600 / $factor;
  readingsBulkUpdate($hash, 's0_2', sprintf("%0.3f", $value));
  
  $factor = $attr{$name}{factor3} || ${hash}->{FACTOR};
  $value = $s0_3 / ($millis / 1000) * 3600 / $factor;
  readingsBulkUpdate($hash, 's0_3', sprintf("%0.3f", $value));
  
  $factor = $attr{$name}{factor4} || ${hash}->{FACTOR};
  $value = $s0_4 / ($millis / 1000) * 3600 / $factor;
  readingsBulkUpdate($hash, 's0_4', sprintf("%0.3f", $value));
  
  $factor = $attr{$name}{factor5} || ${hash}->{FACTOR};
  $value = $s0_5 / ($millis / 1000) * 3600 / $factor;
  readingsBulkUpdate($hash, 's0_5', sprintf("%0.3f", $value));
  
  $factor = $attr{$name}{factor6} || ${hash}->{FACTOR};
  $value = $s0_6 / ($millis / 1000) * 3600 / $factor;
  readingsBulkUpdate($hash, 's0_6', sprintf("%0.3f", $value));
  
  readingsEndUpdate($hash, 1); 
  
  DoTrigger($hash->{NAME}, undef) if($init_done);
}

1;

#----------------------------------------------------------------------

=pod
=begin html

<a name="SNULL"></a>
<h3>SNULL</h3>
<ul>
  Note: this module needs the HTTP::Request and LWP::UserAgent perl modules.
  <br><br>
  <a name="SNULLdefine"></a>
  <b>Define</b>
  <ul>
    <code>define &lt;name&gt; SNULL &lt;ip-address&gt; &lt;port&gt; &lt;delay&gt;</code>
    <br><br>
    Defines an S0 device (Arduino with Ethernet Shield) via ip address. The status of the device is also pooled (delay interval).<br><br>

    Examples:
    <ul>
      <code>define myS0 SNULL 192.168.0.1 80 300</code><br>
    </ul>
  </ul>
  <br>

  <a name="SNULLset"></a>
  <b>Set </b>
  <ul>
    <code>set &lt;name&gt; &lt;value&gt;</code>
    <br><br>
    where <code>value</code> is one of:<br>
    <pre>
    0.00 - 10.00
    </pre>
    Examples:
    <ul>
      <code>set foo 1</code><br>
    </ul>
    <br>
  </ul>
</ul>

=end html
=cut
