//------------------------------------------------------------------------------
/*
 
 S0 Counter v0.01
 
 Reads in the S0 Lines attached on max. 6 pcs. S0 Power Meters.
 
 2014-03-13 juergen.brunk.de@gmail.com
 - initial Version
 
 used Components:
 * Arduino Mega 2560
 * Ethernet shield W5100
 
 Based on Arduino Sample Sketch by Tom Igoe
 Based on ChatServer example by David A. Mellis
 
 */
//------------------------------------------------------------------------------

// 1=DHCP, 0=static - if DHCP takes to long for you, use a static IP instead
#define DHCP 0

#include <SPI.h>
#include <Ethernet.h>

//------------------------------------------------------------------------------

#define VERSION "S0 Counter v0.01"
#define LED     13    // LED already attached to Pin 13
#define SDCARD  4     // Ethernet Shield en/disable SD Card 
#define ETH     10    // Ethernet Shield en/disable W5100 Eth
#define PORT    80    // TCP Port to listen
#define DELAY   50L   // Delay in msec to wait for a new Interrupt Event
#define DEBUG   0     // print out dummy S0 Counters

//------------------------------------------------------------------------------

// MAC address
byte mac[] =
{ 
  0xDE, 0xAD, 0xBE, 0xAF, 0x00, 0x20
};

#if DHCP
IPAddress ip;
#else
// static IP address, gateway and subnet
IPAddress ip(10,3,3,112);
IPAddress gateway(10,3,3,1);
IPAddress subnet(255, 255, 255, 0);
#endif

unsigned long ts_curr = 0L;
unsigned long ts_last = 0L;
unsigned long ts_diff = 0L;

volatile unsigned long s0_counter[] =
{
  0L, 0L, 0L, 0L, 0L, 0L  
};

volatile unsigned long int_last[] =
{
  0L, 0L, 0L, 0L, 0L, 0L  
};

byte s0_idx = 0;
#define s0_max 6
unsigned long val;
byte cnt = 0;

volatile byte blink = 0;

//------------------------------------------------------------------------------

// create new TCP Server
EthernetServer server(PORT);

//------------------------------------------------------------------------------

// called once for initial setup
void setup() 
{

  delay(500);

  // turn LED off
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);

  // turn SDCARD off
  pinMode(SDCARD, OUTPUT);
  digitalWrite(SDCARD, HIGH);

  // init Serial Port
  Serial.begin(9600);

  // print out startup message
  Serial.println("\n\n... starting ...\n");
  Serial.println(VERSION);

#if DHCP
  Serial.println("Trying DHCP");
  if ( ! Ethernet.begin(mac) ) 
  {
    Serial.println("Failed to obtain a DHCP IP, check Link please!");
    while (true)
    {
      // blink fast
      digitalWrite(LED, HIGH);
      delay(100);
      digitalWrite(LED, LOW);
      delay(100);  
    }
  }
#else
  Serial.println("Using static IP address");
  Ethernet.begin(mac, ip, gateway, subnet);
#endif

  // print out local IP address
  Serial.print("local IP address: ");
  Serial.println(Ethernet.localIP());

  Serial.print("bind to TCP Port: "); 
  Serial.println(PORT);
  Serial.println();

  /* 
  setup ISR's
  Interrupts: 0 (on digital pin 2) and 1 (on digital pin 3). 
  Mega has additional four: 2 (pin 21), 3 (pin 20), 4 (pin 19), and 5 (pin 18).
  
  Activate internal Pullups on these inputs to prevent "noise" due to unattached Lines
  S0+ connected by a 470 Ohm Resistor to an Input Pin, S0- connected to ground of Arduino

  Schematic:
                           + Vcc 5V
                           |
                           R    (10-50 kOhm internal Arduino Pullup)
                           |
    S0+ )----R----+-->  )--+--- [Arduino Input Pin (2,3,21,20,19,18)]  
     |    470 Ohm
     |    short protection
      /
     |
     |
    S0- )--------------> )--+
                            |
                            - Gnd
  Signal:

    H ----+  +---
          |  |
    L     +--+  /S0 (Low active)
           
  */

  Serial.println("Setup Input Lines");
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(21, INPUT_PULLUP);
  pinMode(20, INPUT_PULLUP);
  pinMode(19, INPUT_PULLUP);
  pinMode(18, INPUT_PULLUP);
  
  Serial.println("Install Interrupt Service Routines");
  attachInterrupt(0, isr_int0, LOW);
  attachInterrupt(1, isr_int1, LOW);
  attachInterrupt(2, isr_int2, LOW);
  attachInterrupt(3, isr_int3, LOW);
  attachInterrupt(4, isr_int4, LOW);
  attachInterrupt(5, isr_int5, LOW);

  // start listening for clients
  Serial.println("Enter Main Loop");
  Serial.flush();
  server.begin();

}

//------------------------------------------------------------------------------

// called ever and ever again
void loop() 
{

  ts_curr = millis(); // get milliseconds since startup
  ts_diff = ts_curr - ts_last;

  // blink LED if a new S0 Impulse arrived
  if ( blink > 0 )
  {
    blink = 0;
    Serial.println("New S0 Impulse");
    digitalWrite(LED, HIGH);
    delay(50);
    digitalWrite(LED, LOW);
  }

  // new client connection ?
  EthernetClient client = server.available();
  if ( client && (ts_diff > 1000L) )
  {
    Serial.println("new Client Connection");

    // ingore all Data the Client was sending and
    // just send back a minimal HTTP Header and Response
    client.print("HTTP/1.0 200 OK\r\n\n");
    client.print("count: "); 
    client.print(cnt);
    client.print(" millis: "); 
    client.print(ts_diff);
    client.print(" s0: "); 

#if DEBUG
    client.print(random(0,61)); 
    client.print(" ");
    client.print(random(0,61)); 
    client.print(" ");
    client.print(random(0,61)); 
    client.print(" ");
    client.print(random(0,61)); 
    client.print(" ");
    client.print(random(0,61)); 
    client.print(" ");
    client.print(random(0,61)); 
#else
    for ( s0_idx = 0; s0_idx < s0_max; s0_idx++ )
    {
      client.print(s0_counter[s0_idx]);
      client.print(" "); 
      // clear S0 counters[] after read out
      s0_counter[s0_idx] = 0L;
    }  
#endif
    client.println();
    client.flush();
    client.stop();

    cnt++;

    // clear S0 counters[] after read out
/*
    for ( s0_idx = 0; s0_idx < s0_max; s0_idx++ )
    {
      s0_counter[s0_idx] = 0L; 
    }
*/
    ts_last = ts_curr;
  }

}

//------------------------------------------------------------------------------

// ISR's

// pin 2
void isr_int0()
{
  unsigned long int_diff = 0L;
  unsigned long int_curr = 0L;  
  
  int_curr = millis();
  int_diff = int_curr - int_last[0];

  if ( int_diff > DELAY )
  {
    s0_counter[0]++;
    blink++;
  }

  int_last[0] = int_curr;
}

// pin 3
void isr_int1()
{
  unsigned long int_diff = 0L;
  unsigned long int_curr = 0L;  
  
  int_curr = millis();
  int_diff = int_curr - int_last[1];

  if ( int_diff > DELAY )
  {
    s0_counter[1]++;
    blink++;
  }

  int_last[1] = int_curr;
}

// pin 21
void isr_int2()
{
  unsigned long int_diff = 0L;
  unsigned long int_curr = 0L;  
  
  int_curr = millis();
  int_diff = int_curr - int_last[2];

  if ( int_diff > DELAY )
  {
    s0_counter[2]++;
    blink++;
  }

  int_last[2] = int_curr;
}

// pin 20
void isr_int3()
{
  unsigned long int_diff = 0L;
  unsigned long int_curr = 0L;  
  
  int_curr = millis();
  int_diff = int_curr - int_last[3];

  if ( int_diff > DELAY )
  {
    s0_counter[3]++;
    blink++;
  }

  int_last[3] = int_curr;
}

// pin 19
void isr_int4()
{
  unsigned long int_diff = 0L;
  unsigned long int_curr = 0L;  
  
  int_curr = millis();
  int_diff = int_curr - int_last[4];

  if ( int_diff > DELAY )
  {
    s0_counter[4]++;
    blink++;
  }

  int_last[4] = int_curr;
}

// pin 18
void isr_int5()
{
  unsigned long int_diff = 0L;
  unsigned long int_curr = 0L;  
  
  int_curr = millis();
  int_diff = int_curr - int_last[5];

  if ( int_diff > DELAY )
  {
    s0_counter[5]++;
    blink++;
  }

  int_last[5] = int_curr;
}

// EoF







